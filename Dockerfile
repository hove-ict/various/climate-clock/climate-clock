FROM python:3.9-alpine

RUN apk add --no-cache build-base git zlib-dev libjpeg-turbo-dev

RUN pip install pillow nuitka

RUN git clone https://github.com/hzeller/rpi-rgb-led-matrix.git

WORKDIR /rpi-rgb-led-matrix/bindings/python

RUN make build-python PYTHON=$(command -v python3) && \
    make install-python PYTHON=$(command -v python3)

WORKDIR /app

COPY . .

RUN pip install . 

RUN python3 -m nuitka climate_clock --follow-imports --remove-output

CMD ["./climate_clock.bin"]
