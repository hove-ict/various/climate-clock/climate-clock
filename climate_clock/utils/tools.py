import json
import logging
from datetime import datetime
from importlib import resources
from json.decoder import JSONDecodeError
from pathlib import Path
from typing import Dict, Union

import requests
import yaml

import climate_clock.utils.data
import climate_clock.utils.data.fonts

logger = logging.getLogger(__name__)

configdir = Path().cwd().absolute() / "config"


def config(user_config: Union[Path, None] = None) -> Dict[str, str]:
    try:
        with open(configdir / user_config, "r") as config_stream:
            try:
                config = yaml.safe_load(config_stream)
            except yaml.YAMLError as e:
                logger.error(
                    "[!] Unable to load configuration file.",
                    exc_info=e,
                )
                return False
    except FileNotFoundError:
        logger.warning("[!] Config file not found. Using defaults.")
        with resources.open_text(
            climate_clock.utils.data, "config_default.yml"
        ) as config_stream:
            config = yaml.safe_load(config_stream)
    return config


def get_font_path(font_name: str) -> Path:
    font_absolute_path = (
        Path(climate_clock.utils.data.fonts.__file__).parent / f"{font_name}.bdf"
    )
    return str(font_absolute_path)


def get_and_store_latest_carbon_deadline() -> tuple[datetime, bool]:
    try:
        response = requests.get("https://api.climateclock.world/v1/clock")
        data = response.json()["data"]

        cb = {
            "carbon_deadline": data["modules"]["carbon_deadline_1"]["timestamp"],
            "retrieval_timestamp": data["retrieval_timestamp"],
        }

        with open(configdir / "carbon_deadline.json", "w") as file_stream:
            file_stream.write(json.dumps(cb))
        logger.info("[+] Retrieved and stored latest info from API.")
        updated = True
    except Exception as e:
        updated = False
        logger.warning("[-] Unable to update. We'll use the latest we have locally.")
        logger.debug("", exc_info=e)
        try:
            with open(configdir / "carbon_deadline.json", "r") as file_stream:
                cb = json.load(file_stream)
        except JSONDecodeError or FileNotFoundError as e:
            logger.error("[!] Unable to get data, so nothing to display!", exc_info=e)

    return (datetime.fromisoformat(cb["carbon_deadline"]), updated)
