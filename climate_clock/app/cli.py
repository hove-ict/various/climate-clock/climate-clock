import argparse
from pathlib import Path


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(prog="python -m climate_clock")

    def existing_path(string: str) -> Path:
        path = Path(string)
        if path.is_file():
            return path.absolute()
        else:
            parser.error("The provided path does not exist or is not a file.")

    parser.add_argument(
        "-c",
        "--matrix-config",
        help="Path to a custom RGB Matrix configuration file. If omitted, will "
        "use config.yml in current working dir, or resort to default settings.",
        type=existing_path,
        metavar=".YML FILE",
    )
    parser.add_argument(
        "--emulate", help="Run in an RGB Matrix Emulator.", action="store_true"
    )

    args = parser.parse_args()
    return args
