import argparse
import logging
import time
from datetime import datetime, timedelta, timezone

from dateutil.relativedelta import relativedelta
from PIL import ImageColor

from climate_clock.utils.tools import (
    config,
    get_and_store_latest_carbon_deadline,
    get_font_path,
)

start_time = datetime.now(timezone.utc)
UPDATE_ATTEMPTS = 5
UPDATE_INTERVAL = timedelta(seconds=60)

logging.basicConfig(
    format="%(levelname)s: %(message)s",
    level=logging.INFO,
)
logger = logging.getLogger(__name__)

default_matrix_config = {
    "brightness": 100,
    "rows": 32,
    "cols": 64,
    "chain_length": 3,
    "disable_hardware_pulsing": True,
    "gpio_slowdown": 2,
    "hardware_mapping": "adafruit-hat",
    "limit_refresh_rate_hz": 100,
}

RENEWABLES_1 = {
    "initial": 11.4,
    "timestamp": datetime.fromisoformat("2020-01-01T00:00:00+00:00"),
    "rate": 2.0428359571070087e-08,
}


def renewables_1() -> datetime:
    t = (datetime.now(timezone.utc) - RENEWABLES_1["timestamp"]).total_seconds()
    return RENEWABLES_1["rate"] * t + RENEWABLES_1["initial"]


def main(args: argparse.Namespace) -> None:
    if args.emulate:
        from RGBMatrixEmulator import RGBMatrix, RGBMatrixOptions, graphics
    else:
        try:
            from rgbmatrix import RGBMatrix, RGBMatrixOptions, graphics
        except ImportError:
            logger.warning("[!] Unable to import rgbmatrix. Will use emulator.")
            from RGBMatrixEmulator import RGBMatrix, RGBMatrixOptions, graphics

    if matrix_config := config("config.yml"):
        logger.info(f"Loaded config from config/config.yml")
    elif args.matrix_config:
        matrix_config = config(args.matrix_config)
        logger.info(f"Loaded config from {args.matrix_config}")
    else:
        matrix_config = default_matrix_config
        logger.info(f"Loaded default config")

    options = RGBMatrixOptions()
    logger.info("Using the following options for the RGB matrix:")
    for key, value in matrix_config.items():
        setattr(options, key, value)
        logger.info(f"{key}: {value}")

    matrix = RGBMatrix(options=options)
    canvas = matrix.CreateFrameCanvas()

    f1 = graphics.Font()
    f1.LoadFont(get_font_path("10x20"))
    f2 = graphics.Font()
    f2.LoadFont(get_font_path("6x13"))
    f3 = graphics.Font()
    f3.LoadFont(get_font_path("8x13"))
    L1 = 15
    L2 = 30

    red = graphics.Color(*ImageColor.getcolor("#ff0000", "RGB"))
    green = graphics.Color(*ImageColor.getcolor("#00ff00", "RGB"))
    white = graphics.Color(*ImageColor.getcolor("#ffffff", "RGB"))

    updated = False
    update_attempt = 0

    while not time.sleep(0.1):
        now = datetime.now(timezone.utc)

        if update_attempt < UPDATE_ATTEMPTS and not updated:
            if now > start_time + UPDATE_INTERVAL * update_attempt:
                update_attempt += 1
                logger.info(f"[*] Update attempt {update_attempt}...")
                carbon_deadline, updated = get_and_store_latest_carbon_deadline()

        canvas.Clear()

        # Use relativedelta for leap-year awareness
        deadline_delta = relativedelta(carbon_deadline, now)
        years = deadline_delta.years
        # Extract concrete days from the months & days provided by relativedelta
        # @rubberduck: 1. Create a relativedelta object rdays containing Δ months & days
        #              2. Create a concrete time object rdays in the future
        #              3. Create a timedelta object representing that value - now
        #              4. Extract its days
        rdays = relativedelta(months=deadline_delta.months, days=deadline_delta.days)
        days = ((rdays + now) - now).days
        hours = deadline_delta.hours
        minutes = deadline_delta.minutes
        seconds = deadline_delta.seconds
        cs = deadline_delta.microseconds // 10000

        first_line = [
            [f1, red, 1, f"{years:1.0f}"],
            [f3, red, 1, "YEAR " if years == 1 else "YEARS"],
            [f1, red, 1, f"{days:03.0f}"],
            [f3, red, 1, "DAY " if days == 1 else "DAYS"],
            [f1, red, -2, f"{hours:02.0f}"],
            [f1, red, -1, (":", " ")[cs < 50]],
            [f1, red, -2, f"{minutes:02.0f}"],
            [f1, red, -1, (":", " ")[cs < 50]],
            [f1, red, 0, f"{seconds:02.0f}"],
        ]

        x = 1
        for font, color, space, string in first_line:
            x += space + graphics.DrawText(canvas, font, x, L1, color, string)

        # Second line
        # r1 = renewables_1()
        # second_line = [
        #     [f1, green, -2, f"{r1:.0f}"],
        #     [f1, green, -2, f"."],
        #     [f1, green, 3, f"{format(r1, '.9f').split('.')[1]}%"],
        #     [f2, green, 0, "RENEWABLES"],
        # ]

        second_line = [
            [f3, white, 0, " www.climateclock.world"],
        ]

        x = 1
        for font, color, space, string in second_line:
            x += space + graphics.DrawText(canvas, font, x, L2, color, string)

        if updated:
            canvas.SetPixel(0, 0, 100, 0, 0)
        canvas = matrix.SwapOnVSync(canvas)
