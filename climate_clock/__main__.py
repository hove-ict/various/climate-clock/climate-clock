from climate_clock.app.cli import parse_arguments
from climate_clock.app.main import main

if __name__ == "__main__":
    args = parse_arguments()
    main(args)
