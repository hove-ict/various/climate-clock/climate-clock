# Climate Clock

## In the media

- [2022-01-03 - RTV Drenthe](https://www.rtvdrenthe.nl/nieuws/176954/Klimaatactiegroep-Extinction-Rebellion-voert-voor-het-eerst-actie-in-Drenthe)
- [2022-02-07 - Twitter](https://twitter.com/ProvDrenthe/status/1490649487794126850)
- [2022-03-07 - Dagblad van het Noorden](https://dvhn.nl/drenthe/midden-drenthe/Klimaatklok-is-voor-Midden-Drenthe.-Die-in-achterwege-vanwege-oorlog-in-Oekraïne-27522573.html)
- [2022-03-07 - Twitter](https://twitter.com/middendrenthe/status/1500867144262762498)
- [2022-05-02 - De Schakel](https://www.schakel.info/nieuws/algemeen/44541/burgemeester-hiemstra-neemt-drentse-klimaatklok-in-ontvangst)
- [2022-07-03 - RTV Drenthe](https://www.rtvdrenthe.nl/nieuws/14786258/extinction-rebellion-drenthe-voert-actie-in-assen)
- [2022-07-22 - Algemeen Dagblad](https://www.ad.nl/utrecht/rouwceremonie-in-het-centrum-van-utrecht-klimaatklok-verspringt-naar-6-jaar~a53705a3/)

## Development

You can try this out on your development machine without an RGB Matrix display, as it
will start an emulator. You can force that emulator with the flag `--emulate`.

...but you should probably be using: https://github.com/zestyping/cclock
